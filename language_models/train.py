import argparse
import json
import logging
from dataclasses import asdict, dataclass
from pathlib import Path
from typing import Tuple

import pandas as pd  # type: ignore
from sklearn.metrics import confusion_matrix  # type: ignore
from xgboost import XGBClassifier


@dataclass(frozen=True)
class Performance:
    true_positive: int
    true_negative: int
    false_positive: int
    false_negative: int


LABEL = "damaging"
FEATURES = [
    "delta_num_categories",
    "delta_num_headings",
    "delta_num_media",
    "delta_num_refs",
    "delta_num_wikilinks",
    "delta_page_length",
    "user_age",
    "user_editcount",
    "user_is_anonymous",
]


def split_by_language(
    data_df: pd.DataFrame, wiki_db: str
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    language_df = data_df[data_df["wiki_db"] == wiki_db]
    other_languages_df = data_df[data_df["wiki_db"] != wiki_db]
    return language_df, other_languages_df


def train(X: pd.DataFrame, y: pd.Series) -> XGBClassifier:
    logging.info(f"Training gradient boosted classifier: {len(X)} examples")
    model = XGBClassifier()
    model.fit(X, y, verbose=True)
    return model


def calculate_performance(y: pd.Series, y_pred: pd.Series) -> Performance:
    logging.info(f"Calculating model performance metrics: {len(y)} examples")
    tn, fp, fn, tp = confusion_matrix(y, y_pred).ravel()
    logging.info(f"tn: {tn}, fp: {fp}, fn: {fn}, tp: {tp}")
    return Performance(
        true_positive=int(tp),
        true_negative=int(tn),
        false_positive=int(fp),
        false_negative=int(fn),
    )


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--data",
        type=Path,
        required=True,
        help="Path to training data",
    )
    parser.add_argument(
        "--models",
        type=Path,
        required=True,
        help="Directory to write the serialized models",
    )
    parser.add_argument(
        "--performance",
        type=Path,
        required=True,
        help="Directory to write the performance metrics for models",
    )
    parser.add_argument(
        "--predictions",
        type=Path,
        required=False,
        help=(
            "Directory to optionally write the predictions"
            "used for calculating performance"
        ),
    )

    args = parser.parse_args()
    data_path = args.data
    models_dir = args.models
    performance_dir = args.performance
    predictions_dir = args.predictions

    data_df = pd.read_csv(data_path, sep="\t", compression="gzip")
    data_df = data_df[~data_df[LABEL].isnull()]
    data_df[LABEL] = data_df[LABEL].astype(bool)
    logging.info(f"Loaded {len(data_df)} training examples")

    for wiki_db in data_df["wiki_db"].unique():
        logging.info(f"Current language: {wiki_db}")
        language_df, other_languages_df = split_by_language(data_df, wiki_db)
        model = train(other_languages_df[FEATURES], other_languages_df[LABEL])
        language_df["prediction"] = model.predict(language_df[FEATURES])
        model_performance = calculate_performance(
            language_df[LABEL], language_df["prediction"]
        )

        model_path = models_dir / f"{wiki_db}.json"
        logging.info(f"Writing model to {model_path}")
        model.save_model(model_path)

        performance_path = performance_dir / f"{wiki_db}.json"
        logging.info(f"Writing model performance metrics to {performance_path}")
        with open(performance_path, "w") as f:
            json.dump(asdict(model_performance), f)

        if predictions_dir:
            predictions_path = predictions_dir / f"{wiki_db}.tsv"
            logging.info(f"Writing predictions to {predictions_path}")
            language_df[["revision_id", "wiki_db", LABEL, "prediction"]].to_csv(
                predictions_path, index=False, sep="\t"
            )


if __name__ == "__main__":
    main()
